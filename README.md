# Parser obfs4 telegram   
This is a python parser for [telegram channel](https://t.me/s/tor_bridges) (obfs4 bridges for tor) with removing dublicates and replace iat-mode (optional).
You do not need have telegram account for it!

## Installation   
```
git clone https://codeberg.org/1Un1kum/tor_bridges_parser.git
cd tor_bridges_parser
pip3 install -r requirements.txt
python3 parser.py
```
## Functions description
- **Depth:** how many pages will be parsed (There are 20 massages in 1 page)\
- **iat-mode replace:** If you choose `1` or `2` all `iat-mode=0` in bridges will be replaced into selected. If you want not replace `iat-mode` just press `return`.


