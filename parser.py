#!/bin/python3
import requests
from os import system, remove
from bs4 import BeautifulSoup
from rich.console import Console


class Parser:
    def __init__(self, console, filename: str, depth: int, iat_mode: int):
        self.console = console
        self.filename = filename
        self.depth = depth
        self.iat_mode = iat_mode

    def get_page(self, url):
        """Get page via GET request"""
        response = requests.get(url)
        if response.status_code != 200:
            self.console.print(
                f"[bold]Bad response: [red]{response}[/red]. Exiting...")
            exit()

        page = BeautifulSoup(response.text, 'lxml')
        return page

    def get_bridges(self, page):
        """Parse all obfs4 bridges from page"""
        blocks = page.find_all('pre')

        bridges = []
        for block in blocks:
            for string in list(block.stripped_strings):
                if string.find("obfs4") == 0:
                    bridges.append(string)

        return bridges

    def get_next_url(self, page):
        """Get URL for previous page with 20 posts"""
        a = page.find(
            'a', class_="tme_messages_more js-messages_more", href=True)
        href = a['href']
        url = "https://t.me" + href
        return url

    def write(self, bridges: list, filename):
        """Write bridges into file"""
        with open(filename, 'a') as file:
            for b in bridges:
                file.write(b + '\n')

    def remove_dublicates(self, filename):
        self.console.print("[bold cyan]Removing dublicates...")
        system(f"sort tmp.txt | uniq > {self.filename}")

    def remove_tmp(self):
        """Remove tmp file with dublicates"""
        try:
            remove("tmp.txt")
        except OSError:
            pass

    def parse(self):
        url = "https://t.me/s/tor_bridges"

        for i in range(1, self.depth + 1):
            self.console.print(
                f"[bold cyan]Parsing page [yellow]{i}/{self.depth}")
            page = self.get_page(url)
            bridges = self.get_bridges(page)

            # replace iat-mode
            if self.iat_mode != "":
                bridges = list(map(lambda x: x.replace(
                    "iat-mode=0", f"iat-mode={self.iat_mode}"), bridges))

            self.write(bridges, "tmp.txt")
            url = self.get_next_url(page)

        self.remove_dublicates(self.filename)
        self.remove_tmp()


class Dialogs:
    def __init__(self, console):
        self.console = console

    def depth(self):
        answer = console.input(
            "[bold]Select [red]depth[/red] (how many pages) for parse: ").strip()

        if not answer.isdigit() or int(answer) < 1:
            answer = self.ask_depth()

        return int(answer)

    def iat_mode(self):
        answer = console.input(
            "[bold]Which [red]iat-mode[/red] you want?[cyan][1/2/enter - without replace][/cyan]: ").strip()
        variants = ("1", "2", "")

        if answer not in variants:
            answer = self.ask_iat_mode

        return answer


if __name__ == "__main__":
    console = Console(emoji=False, highlight=False)
    dialogs = Dialogs(console)

    # dialogs
    depth = dialogs.depth()
    iat_mode = dialogs.iat_mode()
    print()

    # main
    parser = Parser(console, "bridges.txt", depth, iat_mode)
    parser.parse()
